import pandas as pd
from scipy.spatial.distance import cosine

data = pd.read_csv('data.csv')
data_item_base = data.drop('user', axis=1)
data_item_base_frame = pd.DataFrame(index=data_item_base.columns, columns=data_item_base.columns)

try: 
    data_item_base_frame = pd.read_csv('data_item_base_frame.csv', sep=',', encoding='utf-8')
except:
    for i in range(len(data_item_base_frame.columns)):
        for j in range(len(data_item_base_frame.columns)):
            print(i, " and ", j)
            data_item_base_frame.iloc[i, j] = 1 - cosine(data.iloc[:, i], data.iloc[:, j])
    data_item_base_frame.to_csv('data_item_base_frame.csv', sep=',', encoding='utf-8')

data_neighbors = pd.DataFrame(index=data_item_base_frame.columns, columns=range(1, 11))
for i in range(len(data_item_base_frame.columns)):
    data_neighbors.iloc[i,:10] = data_item_base_frame.iloc[0:, i].sort_values(ascending=False)[:10].index

print(data_neighbors.head(6).iloc[:,0:4])
